#include <iostream>
#include <chrono>
#include <limits.h>
#include "gtest/gtest.h"
#include <sys/resource.h>
#include <unistd.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <streambuf>

#include "myfuncs.h"

using namespace std;

//Accuracy add2Test
TEST(cppTest, add2Test)
{
    EXPECT_EQ(add2(3,4), 7);
    EXPECT_EQ(add2(30,40), 70);
}

//Accuracy mul2Test
TEST(cppTest, mul2Test)
{
    EXPECT_EQ(mul2(3,4), 12);
}

//Accuracy max2Test
TEST(cppTest, max2Test)
{
    EXPECT_EQ(max2(3,4), 4);
}

//Accuracy min2Test
TEST(cppTest, min2Test)
{
    EXPECT_EQ(min2(3,4), 3);
}

//Accuracy min2Test2
TEST(cppTest, min2Test2)
{
    EXPECT_EQ(min2(5,4), 4);
}
